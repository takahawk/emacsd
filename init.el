(require 'package)

(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

(setq git-name "\"takahawk\""
      git-email "\"takahawkkun@gmail.com\"")

(defun install-all ()
  (interactive)
  (progn
    (setq package-selected-packages
	  '(all-the-icons
	    all-the-icons-dired
	    company
	    company-go
	    dired-sidebar
            ggtags
	    go-mode
	    groovy-mode
	    helm
	    helm-projectile
	    json-mode
	    kotlin-mode
	    magit
	    multiple-cursors
	    powerline
	    projectile
	    solidity-mode
	    spacemacs-theme
	    tide))
    (install-external-prerequisites)
    (package-refresh-contents)
    (package-install-selected-packages)
    (all-the-icons-install-fonts)
    (load "~/.emacs.d/init.el")))

(defun install-external-prerequisites ()
    (cond ((eq 0 (shell-command "command -v pacman")) ;; Arch linux
	   (let ((default-directory "/sudo::"))
	     (progn
	      (shell-command "sudo pacman -S --noconfirm git")
	      (shell-command (concat "git config --global user.email " git-email))
	      (shell-command (concat "git config --global user.name " git-name)))))
	  ((eq 0 (shell-command "command -v apt")) ;; Ubuntu/Debian
	   (let ((default-directory "/sudo::"))
	     (progn
	       (shell-command "sudo apt install git"))))))

;; appearance
(load-theme 'spacemacs-dark t)
(menu-bar-mode      -1)
(toggle-scroll-bar  -1)
(tool-bar-mode      -1)

;; custom bindings
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-i")        'imenu)
(global-set-key (kbd "C-M-o") 'other-frame)

;; editing
(electric-pair-mode t)
(show-paren-mode    t)
(setq-default indent-tabs-mode nil)

(defun reverse-input-method (input-method)
  "Build the reverse mapping of single letters from INPUT-METHOD."
  (interactive
   (list (read-input-method-name "Use input method (default current): ")))
  (if (and input-method (symbolp input-method))
      (setq input-method (symbol-name input-method)))
  (let ((current current-input-method)
        (modifiers '(nil (control) (meta) (control meta))))
    (when input-method
      (activate-input-method input-method))
    (when (and current-input-method quail-keyboard-layout)
      (dolist (map (cdr (quail-map)))
        (let* ((to (car map))
               (from (quail-get-translation
                      (cadr map) (char-to-string to) 1)))
          (when (and (characterp from) (characterp to))
            (dolist (mod modifiers)
              (define-key local-function-key-map
                (vector (append mod (list from)))
                (vector (append mod (list to)))))))))
    (when input-method
      (activate-input-method current))))

(reverse-input-method 'russian-computer)


;; ido mode
(setq ido-enable-flex-matching       t)
(setq ido-everywhere                 t)
(setq ido-create-new-buffer    'always)
(setq ido-file-extensions-order
           '(".el" ".kt" ".java" ".c"))
(setq ido-ignore-extensions          t)
(ido-mode                            t)


;; movement
(setq next-screen-context-lines 5)
(setq line-move-visual        nil)
(global-subword-mode            t)
(global-superword-mode         -1)
(setq shift-select-mode       nil)

;; kill ring
(setq kill-do-not-save-duplicates t)


;; PACKAGE CONFIGURATION
;; all the icons
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

;; c-mode
(add-hook 'c-mode-hook (lambda ()
                         (progn
                           (setq c-basic-offset 4
								 tab-width 4
								 indent-tabs-mode t))))


;; company mode
(add-hook 'c-mode-hook 'company-mode)
(add-hook 'emacs-lisp-mode-hook 'company-mode)
(add-hook 'go-mode-hook 'company-mode)
(add-hook 'java-mode-hook 'company-mode)

;; dired-sidebar
(global-set-key (kbd "C-M-;") 'dired-sidebar-toggle-sidebar)

;; ggtags
(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))

;; helm
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-b") 'helm-buffers-list)
(global-set-key (kbd "M-i") 'helm-imenu)
(global-set-key (kbd "C-x r l") 'helm-bookmarks)

;; Kotlin
(setq kotlin-tab-width 4)

;; java-mode
(add-hook 'java-mode-hook (lambda ()
			    (setq c-basic-offset 2)))

;; magit
(global-set-key (kbd "C-x g") 'magit-status)


;; Multiple Cursors
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C-+") 'mc/mark-pop)



;; org mode
(setq org-todo-keywords
      '((sequence "TODO" "IN PROGRESS" "|" "DONE")))

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-switchb)


;; Powerline
(powerline-default-theme)


;; Projectile
(global-set-key (kbd "C-c p f") 'helm-projectile-find-file)
(global-set-key (kbd "C-c p h") 'helm-projectile)
(global-set-key (kbd "C-c p s g") 'helm-projectile-grep)


;; Tide (TypeScript)
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  (company-mode +1))

(add-hook 'typescript-mode-hook #'setup-tide-mode)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" default)))
 '(package-selected-packages
   (quote
    (json-mode helm sublimity spacemacs-theme magit dired-sidebar dakrone-theme all-the-icons-dired))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
